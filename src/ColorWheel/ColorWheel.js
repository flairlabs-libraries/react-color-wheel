import React, { Component } from 'react';
import equal from 'fast-deep-equal'
import "./ColorWheel.css";
export default class ColorWheel extends Component {
    constructor() {
        super();
        this.expandedCoreCanvasRef = React.createRef();
        this.coreCanvasRef = React.createRef();
        this.drawingCanvasRef = React.createRef();
        this.selectedPie = undefined;
        this.hoveredPie = undefined;
        this.onceHovered = false;
        this.onceSelected = false;
    }
    componentDidMount() {
        this.resizeCanvasToFitContainer(this.expandedCoreCanvasRef.current);
        this.resizeCanvasToFitContainer(this.coreCanvasRef.current);
        this.resizeCanvasToFitContainer(this.drawingCanvasRef.current);
        this.updatePropsCopy();
        this.canvas = this.drawingCanvasRef.current;
        let width = this.drawingCanvasRef.current.width;
        this.innerWidth = (width * 0.95) / 6;
        this.middleWidth = (width * 0.95) / 3;
        this.outerWidth = (width * 0.95) / 2;
        this.drawingCtx = this.drawingCanvasRef.current.getContext("2d");
        this.colors = this.props.colors;
        this.totalColorQuantity = this.colors.map(color => color.percent).reduce((p1, p2) => p1 + p2, 0);
        /**
         * * Draw inner & outer wheel which doesn't requires changes
         */
        this.drawInnerRing(this.coreCanvasRef.current.getContext("2d"));
        this.drawOuterRing(this.expandedCoreCanvasRef.current.getContext("2d"))

    }
    updatePropsCopy(){
        this.propsCopy = this.getRequiredProps(this.props);
    }
    /**
     * 
     * @param {Props} props 
     * @returns Required Props value out of all props
     */
    getRequiredProps(props){
        const requiredProps = ['colors', 'expandAll', 'showPercent','font','colorView'];
        let a = {};
        requiredProps.forEach(key=>{
            a[key] = typeof props[key] === 'object'? {...props[key]}: props[key];
        })
        return a;
    }
    componentDidUpdate(){
        if(!equal(this.getRequiredProps(this.props),this.propsCopy)){
            this.drawInnerRing(this.coreCanvasRef.current.getContext("2d"));
            this.drawOuterRing(this.expandedCoreCanvasRef.current.getContext("2d"));
            this.refreshDrawingCanvasOnInteraction();
            this.updatePropsCopy();
        }
    }
    /**
     * 
     * @param {CanvasRenderingContext2D} ctx 
     * @param {Number} shadowBlur
     * @param {String} color 
     * * Util Function: Changes shadow effect
     */
    toggleShadow(ctx = this.drawingCtx ,shadowBlur = 0, color = 'black'){
        ctx.shadowColor = color;
        ctx.shadowBlur = shadowBlur;
    }
    /**
     * 
     * @param {CanvasRenderingContext2D} ctx 
     * @param {String} text 
     * @param {Number} x 
     * @param {Number} y 
     * @param {String} color 
     * * Util Function: Draw color quantity in percentage Or in quantity 
     */
    drawText(ctx, text, x, y, color){
        let font = [];
        font.push(this.props.font.fontStyle?this.props.font.fontStyle:'normal');
        font.push(this.props.font.fontVarient?this.props.font.fontVarient:'normal');
        font.push(this.props.font.fontWeight?this.props.font.fontWeight:'normal');
        font.push(this.props.font.fontSize?this.props.font.fontSize+'px':'15px');
        font.push(this.props.font.fontFamily?this.props.font.fontFamily:'Arial');
        ctx.font = font.join(' ');
        ctx.fillStyle = this.invertColor(color,true);
        ctx.textAlign = "center";
        let textToDraw = this.props.showPercent? Math.round(((text/this.totalColorQuantity) * 100)) + "%": text;
        ctx.fillText(textToDraw, x , y+this.props.font.fontSize/2);
    }
    /**
     * 
     * @param {CanvasRenderingContext2D} ctx 
     * @param {Object} color 
     * * Util Function: Draw text on Pie. 
     * * Color Object {color: string, startAngle: float, endAngle: float, percent: Number}
     */
    drawPieText(ctx,color){
        this.toggleShadow(ctx,0);
        let theta = color['startAngle']+(color['endAngle'] - color['startAngle'])/2;
        let coordinates = this.calculateCartesian(this.middleWidth * 0.75,theta)
        this.drawText(ctx, color.percent, coordinates.x, coordinates.y, color.color);
    }
    /**
     * 
     * @param {CanvasRenderingContext2D} ctx 
     * @param {Number} length 
     * @param {Object} color 
     * * Util Function: Draws Pie with text and shadow if mouse hovered. 
     * * It redraws Center circle.
     */
    drawArc(ctx, length,color){
        ctx.fillStyle = color.color;
        ctx.beginPath();
        if(this.hoveredPie && this.hoveredPie.color.color === color.color){
            this.toggleShadow(ctx, 10);
        }else{
            this.toggleShadow(ctx, 0);
        }
        ctx.arc(this.canvas.width / 2, this.canvas.height / 2, length, color.startAngle, color.endAngle, false);
        ctx.arc(this.canvas.width / 2, this.canvas.height / 2, length/2, color.endAngle, color.startAngle, true);
        ctx.fill();
        if(color.child && color.child.length > 0 && (color.showSize || (this.hoveredPie && this.hoveredPie.color.color === color.color)))
            this.drawPieText(ctx, color)
        this.toggleShadow(ctx,0);
    }
    /**
     * 
     * @param {CanvasRenderingContext2D} ctx 
     * * Draws Static inner ring
     */
    drawInnerRing(ctx){
        ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        let lastendAngle = 0;
        for(let i = 0; i< this.colors.length; i++){
            ctx.moveTo(this.canvas.width / 2, this.canvas.height / 2);
            let currentAngle = (Math.PI * 2 * (this.colors[i].percent / this.totalColorQuantity));
            this.colors[i]['startAngle'] = lastendAngle;
            this.colors[i]['endAngle'] = lastendAngle += currentAngle;
            /**
             * Checking if given font size can fit in  Pie or not
             */
            let startAngleCartesianCoordinate = this.calculateCartesian(this.middleWidth/2, this.colors[i]['startAngle']);            
            let endAngleCartesianCoordinate = this.calculateCartesian(this.middleWidth/2, this.colors[i]['endAngle']);
            var a = startAngleCartesianCoordinate.x - endAngleCartesianCoordinate.x;
            var b = startAngleCartesianCoordinate.y - endAngleCartesianCoordinate.y;
            let distance = Math.sqrt( a*a + b*b );
            this.colors[i]['length'] = this.middleWidth;
            this.colors[i]['showSize'] = distance > this.props.font.fontSize;
            this.drawArc(ctx,this.middleWidth,this.colors[i]);
        }
    }
    /**
     * 
     * @param {CanvasRenderingContext2D} ctx 
     * * Draws Static Outer ring
     */
    drawOuterRing(ctx = undefined){
        ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        let childPieCount = 0;
        let maxLength = this.outerWidth - this.middleWidth;
        for(let i = 0; i< this.colors.length; i++){
            ctx.moveTo(this.canvas.width / 2, this.canvas.height / 2);
            let childLastendAngle = this.colors[i]['startAngle'];
            let step = (maxLength)/this.colors[i].child.length;
            let startLength = this.middleWidth+step;
            let childMinAngle = Math.min(...this.colors[i].child.map(c => c.percent));
            let childMaxAngle = this.colors[i].percent;
            
            for(let j = 0; j< this.colors[i].child.length; j++){
                let currentChildAngle = Math.PI * 2 * (this.colors[i].child[j].percent / this.totalColorQuantity);
                this.colors[i].child[j]['startAngle'] = childLastendAngle;
                this.colors[i].child[j]['endAngle'] = childLastendAngle += currentChildAngle;
                if(this.props.colorView === 'gear'){
                    if(childPieCount%2===0){
                        this.colors[i].child[j]['length'] = this.outerWidth*0.9;
                    }else{
                        this.colors[i].child[j]['length'] = this.outerWidth;
                    }
                }else if(this.props.colorView === 'wheel'){
                    this.colors[i].child[j]['length'] = this.outerWidth;
                }else if(this.props.colorView === 'blade'){
                    this.colors[i].child[j]['length'] = startLength;
                    startLength+=step;
                }else if(this.props.colorView === 'snowflake'){
                    let childAnglePercentage = this.getPercentageByRange(childMinAngle, childMaxAngle, this.colors[i].child[j].percent);
                    this.colors[i].child[j]['length'] = this.middleWidth + maxLength * (1 - childAnglePercentage);
                }
                this.drawArc(ctx, this.colors[i].child[j]['length'], this.colors[i].child[j]);
                childPieCount++;
            }
        }
    }
    getPercentageByRange(min, max, input){
        return (input - min) / (max - min);
    }
    /**
     * Important: This Method gets called on every Mouse(Click & Hover) Interactions.
     * It Refreshed the Drawing canvas and Redraws Selected Pie , Hovered Pie & Center Cirle everytime.
     */
    refreshDrawingCanvasOnInteraction(){
        this.drawingCtx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        if(this.selectedPie){
            let childLastendAngle = this.selectedPie['startAngle'];
            for(let j = 0; j< this.selectedPie.child.length; j++){
                let currentChildAngle = Math.PI * 2 * (this.selectedPie.child[j].percent / this.totalColorQuantity);
                this.selectedPie.child[j]['startAngle'] = childLastendAngle;
                this.selectedPie.child[j]['endAngle'] = childLastendAngle += currentChildAngle;
                this.drawArc(this.drawingCtx, this.selectedPie.child[j]['length'], this.selectedPie.child[j]);
            }
            this.drawArc(this.drawingCtx, this.selectedPie['length'], this.selectedPie);
        }
        if(this.hoveredPie){
            this.drawArc(this.drawingCtx, this.hoveredPie.color['length'], this.hoveredPie.color);
            if(this.hoveredPie.isChildSelected){
                this.drawArc(this.drawingCtx, this.hoveredPie.parentColor['length'], this.hoveredPie.parentColor);
                this.drawPieText(this.drawingCtx, this.hoveredPie.parentColor);
            }
        }
        
    }
    /**
     * 
     * @param {Mouse Hover} event
     * * It Refresh the Drawing canvas And Draws hovered pie with shadow.
     * * Note: It will only refresh canvas when previous hovered & current
     * * hovered pie are not same.
     */
    canvasHover(event) {
        let coordinates = this.getCursorPosition(event);
        let polarCoordinate = this.getPolarCoordinate(this.canvas.height / 2, this.canvas.height / 2, coordinates.x, coordinates.y)
        let hoveredColor = this.getColorByCoordinates(polarCoordinate.theta, polarCoordinate.distance);
        if (hoveredColor && (!this.hoveredPie || (this.hoveredPie && hoveredColor.color.color !== this.hoveredPie.color))) {
            this.hoveredPie = hoveredColor;
            this.refreshDrawingCanvasOnInteraction();
        } else if (!hoveredColor) {
            this.hoveredPie = undefined;
            this.refreshDrawingCanvasOnInteraction();
        }
    }
    /**
     * 
     * @param {Mouse Click} event 
     * * It Refresh the Drawing canvas And Draws Expanded color pie with text.
     * * Note: It will only refresh canvas when previous selected & current
     * * selected pie are not same. It also emits onColorSelect event.
     */
    canvasClick(event) {
        let coordinates = this.getCursorPosition(event);
        let polarCoordinate = this.getPolarCoordinate(this.canvas.height / 2, this.canvas.height / 2, coordinates.x, coordinates.y)
        let selectPie = this.getColorByCoordinates(polarCoordinate.theta, polarCoordinate.distance);
        if(selectPie){
            this.props.onColorSelect(selectPie.color); 
            if(!selectPie.isChildSelected && (!this.selectedPie || (this.selectedPie && selectPie.color.color !== this.selectedPie.color))){
                this.selectedPie = selectPie.isChildSelected? {...selectPie.parentColor} : {...selectPie.color};
                this.refreshDrawingCanvasOnInteraction();
            }
        }
    }
    /**
     * 
     * @param {Number} cx 
     * @param {Number} cy 
     * @param {Number} ex 
     * @param {Number} ey 
     * @returns Polar co-ordinates (angle & distance from circle center point) 
     * from caartesian co-ordinates
     */
    getPolarCoordinate(cx, cy, ex, ey) {
        let dy = ey - cy;
        let dx = ex - cx;
        let theta = Math.atan2(dy, dx);
        theta = theta > 0 ? theta : (2 * Math.PI + theta)
        var distance = Math.sqrt(dy * dy + dx * dx);
        return { theta, distance }
    }
    getCursorPosition(event) {
        const rect = this.canvas.getBoundingClientRect();
        const x = event.clientX - rect.left
        const y = event.clientY - rect.top
        return { x, y };
    }
    /**
     * 
     * @param {Number} selectedAngle 
     * @param {Number} length 
     * @returns Color (Main color where user clicked/hovered) ,
     *  Parent Color (If user clicked/hovered child color) 
     */
    getColorByCoordinates(selectedAngle, length) {
        let idx = 0;
        let color;
        let parentColor = undefined;
        let isChildSelected = false;
        if (length < this.innerWidth || length > this.outerWidth) {
            return
        } else {
            if(length > this.middleWidth && !this.selectedPie && !this.props.expandAll){
                return
            }
            while (idx < this.colors.length && this.colors[idx].startAngle <= selectedAngle) {
                idx++
            }
            color = { ...this.colors[idx - 1] };
            if (length >= this.middleWidth && length <= this.outerWidth) {
                if (this.selectedPie && this.selectedPie.color !== color.color && !this.props.expandAll) return;
                if ((this.selectedPie && this.selectedPie.color === color.color) || this.props.expandAll) {
                    parentColor = { ...color };
                    let idxx = 0;
                    while (idxx < color.child.length && color.child[idxx].startAngle <= selectedAngle) {
                        idxx++;
                    }
                    color = { ...color.child[idxx - 1] };
                    isChildSelected = true;
                }
            }
            return { color, parentColor, isChildSelected };

        }
    }
    /**
     * 
     * @param {Number} r 
     * @param {Number} theta 
     * @returns Cartesian co-ordinates (x,y from canvas's top left as origin)
     *  from polar co-ordinate
     */
    calculateCartesian(r, theta) {
        var x = r * Math.cos(theta);
        var y = r * Math.sin(theta);
        return {
            x: this.canvas.width / 2 + x,
            y: this.canvas.width / 2 + y
        }
    }
    /**
     * 
     * @param {String} hex 
     * @param {Boolean} bw 
     * @returns Inverted color from hex color
     * Note: bw is Black & White, If passed true , 
     * It will return only black & white color with good contrast.
     */
    invertColor(hex, bw) {
        if (hex.indexOf('#') === 0) {
            hex = hex.slice(1);
        }
        if (hex.length === 3) {
            hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
        }
        if (hex.length !== 6) {
            throw new Error('Invalid HEX color.');
        }
        var r = parseInt(hex.slice(0, 2), 16),
            g = parseInt(hex.slice(2, 4), 16),
            b = parseInt(hex.slice(4, 6), 16);
        if (bw) {
            return (r * 0.299 + g * 0.587 + b * 0.114) > 186
                ? '#000000'
                : '#FFFFFF';
        }
        r = (255 - r).toString(16);
        g = (255 - g).toString(16);
        b = (255 - b).toString(16);
        return "#" + this.padZero(r) + this.padZero(g) + this.padZero(b);
    }
    padZero(str, len) {
        len = len || 2;
        var zeros = new Array(len).join('0');
        return (zeros + str).slice(-len);
    }
    /**
     * 
     * @param {HTMLCanvasElement} canvas 
     */
    resizeCanvasToFitContainer(canvas){
        canvas.style.width = "100%";
        canvas.style.height = "100%";
        let w = canvas.offsetWidth;
        let h = canvas.offsetHeight;
        if(w >= h){
            canvas.width = h;
            canvas.height = h;
            canvas.style.width = h+'px';
            canvas.style.height = h+'px';
        }else{
            canvas.width = w;
            canvas.height = w;       
            canvas.style.width = w+'px';
            canvas.style.height = w+'px';     
        }
        
    }
    render() {
        return (
            <div className="wheel-container">
                <div className="canvas-container">
                    <canvas style={{ zIndex: 0, visibility: this.props.expandAll ? 'visible' : 'hidden' }} ref={this.expandedCoreCanvasRef} ></canvas>
                    <canvas style={{ zIndex: 1 }} ref={this.coreCanvasRef} ></canvas>
                    <canvas style={{ zIndex: 2 }} ref={this.drawingCanvasRef} 
                        onMouseMove={this.canvasHover.bind(this)}
                        onMouseDown={this.canvasClick.bind(this)}></canvas>
                </div>
                {this.props.showLegend && 
                    <div className="legends-container">
                        {
                            this.props.colors.map(color => <div className="legend"><div  className="legend-cicle" style={{backgroundColor:color.color, border:'1px solid '+this.invertColor(color.color,true)}}></div><span className="legend-text">{color.name}</span></div>)
                        }
                    </div>
                }
            </div>
        )
    }
}